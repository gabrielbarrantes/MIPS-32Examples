.text 
main: # label for main
	lw $t0, PAL
	la $t1, DIRACU
	li $t3, 0 #i
	li $t7, 0 #carry

	li $v0, 4
	la $a0, dirac # load adress
	syscall
printLoopDirAcu:
	sltu $t4, $t3, $t0
	beq $t4, $zero, loop0

	sll $t4, $t3, 2	 
	add $t5, $t4, $t1 # $t5=(DIRACU[i])

	lw $t6,0($t5) # $t6=DIRACU[i]

	li $v0, 1
	move $a0, $t6
	syscall

	li $v0, 4
	la $a0, comma
	syscall

	addi $t3, $t3, 1
	j printLoopDirAcu


loop0:
	li $t3, 0 #i
	li $t7, 0 #carry
	li $t2,0x80000000 #Mask
loop:
	sltu $t4, $t3, $t0
	beq $t4, $zero, endSum

	sll $t4, $t3, 2	 
	addu $t5, $t4, $t1 

	lw $t6,0($t5) 
	and $t4, $t6, $t2
	slt $t4, $t4, $zero

	sll $t6,$t6,1
	or $t6, $t6, $t7

	move $t7, $t4

	sw $t6, 0($t5)
	addi $t3, $t3, 1
	j loop

endSum:
	li $t3, 0
	lw $t0, PAL

	li $v0, 4
	la $a0, dirac # load adress
	syscall
printLoopDirAcu2:


	sltu $t4, $t3, $t0
	beq $t4, $zero, endProg

	sll $t4, $t3, 2	 
	add $t5, $t4, $t1 # $t5=(DIRACU[i])

	lw $t6,0($t5) # $t6=DIRACU[i]

	li $v0, 1
	move $a0, $t6
	syscall

	li $v0, 4
	la $a0, comma
	syscall

	addi $t3, $t3, 1
	j printLoopDirAcu2

endProg:
	li $v0, 10
	syscall


.ktext 0x80000180
	mfc0 $k0, $14
	addi $k0, $k0,4
	jr $k0


.data # declare data segment
	PAL:.word 4 # initialize x to 10
	DIRACU:.word 4, 2, 0x80000000, 1
	comma: .asciiz ","
	dirac: .asciiz "\nDIRACU: "
