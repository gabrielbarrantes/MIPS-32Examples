#######################################################
# Gabriel Barrantes Rodas B30904 grupo 1              #
# Tarea2: para implementar el algoritmo bubblesort se #
# empleo un esquema de dos estructuras "for" anidadas #
# en un procedimiento q recibe dos parametros, el     #
# largo y la direccion del vector.                    #
# Y para la impresion del arreglo se uso una          #
# estructura for que recibe tambien el largo y la     #
# direccion.                                          #
# en el bloque main se llama a ambos procedimientos   #
# y se observa la salida en consola                   # 
#######################################################
.text # declare text segment
#Procedimiento que recibe en a0 la direccion del
#vector de enteros a imprimir y en a1 su longitud
BubbleSort: 
    move $t0,$a1     #cargo en t0 la longitud
    add  $t0,$t0,-1  #y la acondiciono para que
    sll  $t0,$t0, 2  #apunte al final del vector
    add  $t0,$t0,$a0 #a ordenar
    beq  $t0,$a0,fin_BubbleSort #si el final es el inicio,
                                #(largo 1) terminamos
loop1_BubbleSort:
    beq  $t0,$a0,fin_BubbleSort #t0 apunta a la direccion
    move $t1,$a0                #donde hay que terminar las
  loop2_BubbleSort:             #comparaciones
        beq  $t0,$t1,salida_BubbleSort
    	lw   $t3,0($t1)         #mientras que t1 apunta a la 
   	 	lw   $t4,4($t1)         #direccion de la actual comparacion
    	sub  $t5,$t3,$t4        #resta para determinar cual es mayor
    	bgez $t5,noSwap         #se hace el swap si es necesario
    	sw   $t4,0($t1)
    	sw   $t3,4($t1)
    	noSwap:                 #si no se sigue con la siguiente
    	addi $t1,$t1,4          #comparacion
    	j    loop2_BubbleSort
  salida_BubbleSort:
  addi $t0,$t0,-4               #decremento de la direccion de la
  j    loop1_BubbleSort         #ultima comparacion
fin_BubbleSort: jr $ra

#Procedimiento que recibe en a0 la direccion del
#vector de enteros a imprimir y en a1 su longitud
ImprimeVector:
    move $t1,$a0 #direccion de inicio de impresion

    li   $v0, 1 #Imprime el primer entero
	lw   $a0, 0($t1)
	syscall
	#Cargar en t0 la direccion de la ultima 
	#palabra a imprimir
	addi $t0,$a1,-1
    sll  $t0,$t0, 2
    add  $t0,$t0,$t1
    beq  $t0,$t1,fin_ImprimeVector#salir si la ultima impresion
                                  #es la primera (longitud 1)
#se entra al loop si la longitud es 2 o mas
loop_ImprimeVector:
	la   $a0,coma     #impresion de la coma
	li   $v0,4
	syscall

    addi $t1,$t1, 4   #direccion del sig entero a imprimir
	
	li   $v0, 1       # Impresion del entero siguiente
	lw   $a0, 0($t1)
	syscall

    beq  $t0,$t1,fin_ImprimeVector #si llegamos al final salimos
    j    loop_ImprimeVector        #si no repetimos
fin_ImprimeVector: 
	jr $ra


main:
####Paso de parametros y llamada al 
####procedimiento de ordenado
	la   $a0, Data   #direccion del vector
	li   $a1, 25     #tamano del vector
	jal  BubbleSort
####Paso de parametros y llamada al 
####procedimiento de impresion	
	la   $a0, Data   #direccion del vector
	li   $a1, 25     #tamano del vector
	jal  ImprimeVector
#############################################
	li $v0, 10 		 # codigo de llamada para terminar
	syscall 		 # llamada al sistema


.data # declaracion del segmento de datos
	Data: .word 87, 216, -54, 751, 1, 36, 1225, -446, -6695, -8741, 101, 9635, -9896, 4, 2008, -99, -6, 1, 544, 6, 0, 7899, 74, -42, -9
	coma: .asciiz ", "