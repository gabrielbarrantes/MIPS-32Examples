#######################################################
# Gabriel Barrantes Rodas B30904 grupo 1              #
# Tarea4: Se implementa el algoritmo de la division   #
# usado el algoritmo del libro de texto               #
#######################################################
.text # declare text segment

##################################
#         Rutina divisora        #
##################################
dividir: #recibe dos enteros con signo en $a0 y $a1
         #y devuelve el cociente de $a0/$a1 en $v0 y
         #en $v1 el residuo
    li   $t0, 0x80000000 
    and  $t6, $a0, $t0 #signo de $a0 y del residuo
    and  $t7, $a1, $t0 #signo de $a1

    beq   $t6, $zero, noComplementar_a0 #si a0 es negativo 
    nor   $a0,$a0,$zero   #se niega y suma 1 
    addiu $a0,$a0, 0x1
noComplementar_a0:
    beq   $t7, $zero, noComplementar_a1 #si a1 es positivo
    nor   $a1,$a1,$zero   #se niega y suma 1 
    addiu $a1,$a1, 0x1
noComplementar_a1:
	xor  $t7, $t7,$t6  #signo del cociente

########################################
##  Division como si fueran positivos ##
########################################
    move $v0, $a0  #puesta del residuo inicial (a0) en $v0
    li   $v1, 0x0  #puesta a cero la parte alta (v1)

    li   $t0, 0x0  #contador de iteraciones en cero
    li   $t1, 0x20 #cantidad de iteraciones (32) 
    li   $t2, 0x80000000#mascara para rotado a la izq

loop_dividir:      #loop para restas sucesivas
########################
########################
    li   $t3, 0x0  #resetar el bit q se cargara a Q en el rotado 
    blt  $v1,$a1,noResta#si el divisor es mas grande q el cociente
    sub  $v1,$v1,$a1    #no restamos, si no si restamos y 
    li   $t3, 0x1       #seteamos el bit de entrada al cociente
noResta:
    and  $t4,$v0,$t2 #extraemos el msb de v0
    srl  $t4,$t4, 0x1F#lo rotamos a la posicion menos significaiva
    sll  $v1,$v1, 0x1 #rotamos v1 a la izq
    or   $v1,$v1,$t4  #metemos el msb de v0 a el lsb de v1
    sll  $v0,$v0, 0x1 #rotamos a la izq v0
    or   $v0,$v0, $t3 #y metemos un 1 en Q si se hizo la resta, si no se mete un cero
########################
########################
    addi $t0,$t0, 0x1 #aumentamos el contador
    beq  $t0,$t1,fin_dividir0 #si se hicieron 32 iteraciones salimos
    j    loop_dividir # si no regresamos al loop

fin_dividir0: #aqui se hace el paso final del algoritmo
    li   $t3, 0x0#pues solo se rota v0 si se hace la resta, y v1 no se rota
    blt  $v1,$a1,noResta2
    sub  $v1,$v1,$a1
    li   $t3, 0x1
noResta2:
    sll  $v0,$v0, 0x1
    or   $v0,$v0, $t3

###    Arreglar signo de las salidas  ###

    beq  $t7,$zero,fin_dividir1#se le pone a v0 el signo del
    nor  $v0,$v0,$zero         #resultado q teniamos en t7 
    addu $v0,$v0, 0x1
fin_dividir1:          #se le pone a v1 (residuo) el signo de
	beq  $t6,$zero,fin_dividir2
	nor  $v1,$v1,$zero #a0 (el dividendo) q teniamos almacenado en t6  
    addu $v1,$v1, 0x1
fin_dividir2: jr $ra

################################
################################
#####                      #####
#####         MAIN         #####
#####                      #####
################################
################################
main:

    # Lectura del primer valor
    li $v0, 4
    la $a0, xread
    syscall
    # INT Read
    li $v0, 5
    syscall
    move $t0,$v0  #guardado en t0 del valor leido
    ##########################
    # Lectura del segundo valor
    li $v0, 4
    la $a0, yread # load adress
    syscall

    # INT Read
    li $v0, 5
    syscall

    move $t1,$v0  #guardado en t1 del valor leido
    ###########################
    move  $a0,$t0 #paso de los parametros para el
    move  $a1,$t1 #llamado a la multiplicacion

    jal   dividir #llamado a la rutina multiplicadora

    move  $t0,$v0 #mover los resultados a t0 y t1
    move  $t1,$v1

    # Impresion de los resultados
    #############################
    li $v0, 4
    la $a0, resultado # load adress
    syscall

    li $v0, 4
    la $a0, hi # load adress
    syscall

    # Print Hi
    li   $v0, 1
    move $a0, $t1
    syscall 

    # Print String
    li $v0, 4
    la $a0, lo 
    syscall

    # Print Lo
    li   $v0, 1
    move $a0, $t0
    syscall
    #############################
    #############################

    li $v0, 10      # call code for terminate
    syscall         # system call   


.data # declare data segment
    hi:       .asciiz "\n Hi "
    lo:       .asciiz "\n Lo "
    xread:    .asciiz "\n Ingrese el valor de x: "
    yread:    .asciiz "\n Ingrese el valor de y: "
    resultado:.asciiz "\n El residuo y cociente de x/y son :\n "