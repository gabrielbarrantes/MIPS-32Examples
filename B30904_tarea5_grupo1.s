#######################################################
# Gabriel Barrantes Rodas B30904 grupo 1              #
# Tarea5: Se implemento un procedimiento para calcular#
# una hipotenusa dados los catetos, usando el         #
# newtoniano para aproximar la raiz, mediante la      #
# unidad punto flotante. 
#######################################################
#ddd
.text # declare text segment

#######################################
#### Calculadora de Raiz cuadrada #####
#######################################

raiz:#procedimiento q recibe un x positivo, punto flotante en $f4
	 #y devuelve la raiz en $f2, calculada con el metodo newtoniano
	addi    $t2,  $zero, 0x2 #cargado del 2 usado
	mtc1    $t2,  $f10       #en el algoritmo
	cvt.s.w $f10, $f10       #y convertido en flotante
	li    $t0, 0x0			 #inicio de contador
	li    $t1, 0x14          # maxima cantidad de iteraciones
	div.s $f2, $f4, $f10     #semilla inicial = N/2
###########
loop_raiz:
	div.s $f9, $f4, $f2
	add.s $f2, $f2, $f9
	div.s $f2, $f2, $f10
	addi  $t0, $t0, 0x1
	beq   $t0, $t1, fin_raiz
	j     loop_raiz   
###########
fin_raiz: jr $ra 
#######################################
###### Calculadora de Hipotenusa  #####
#######################################
hipotenusa:#recibe en $f4, y $f5 los catetos y devuelve en $v0 la hipotenusa
	mul.s $f4,$f4,$f4  #eleva al cuadrado los catetos
	mul.s $f5,$f5,$f5
	add.s $f4,$f4,$f5  #los suma
	addi  $sp,$sp, -4  #apilamos $ra
	sw    $ra, 0($sp)  
	jal   raiz         #llamamos a raiz
	mfc1  $v0,$f2      #pasamos el resultado de raiz a $v0
	lw    $ra,0($sp)   #desapilamos $ra
	addi  $sp,$sp, 4   
	jr    $ra          #retornamos

main: # label for main

	li  $v0, 4
	la  $a0, out1 # carga direccion del string
	syscall

	lwc1 $f4, n1
	lwc1 $f5, n2

	jal hipotenusa
	mtc1 $v0,$f12 
	li $v0, 2
	syscall
###################
	li  $v0, 4
	la  $a0, out2 # carga direccion del string
	syscall

	lwc1 $f4, n3
	lwc1 $f5, n4

	jal hipotenusa
	mtc1 $v0,$f12
	li   $v0, 2
	syscall


main_loop:

	# Impresion del mensaje de solicitud de entrada
	li $v0, 4
	la $a0, catetoRead0 # carga direccion del string
	syscall

	# Lectura del cateto 0 (flotante) 
	li $v0, 6
	syscall
	mov.s $f4,$f0

	# Impresion del mensaje de solicitud de entrada
	li $v0, 4
	la $a0, catetoRead1 # carga direccion del string
	syscall

	# Lectura del cateto 1 (flotante) 
	li $v0, 6
	syscall
	mov.s $f5,$f0

	jal hipotenusa
	mtc1 $v0,$f12

	li  $t0, 0x7F800000    #verificamos si es inifito o un NAN la salida
	and $t1, $t0, $v0
	bne $t0, $t1, no_infinity
	li  $v0, 4
	la  $a0, infinityPrint # carga direccion del string
	syscall
	j main_loop

no_infinity:
	# Impresion de la hipotenusa (flotante)
	# Impresion del mensaje de salida
	li $v0, 4
	la $a0, hipotenusaPrint # carga direccion del string
	syscall
	#######
	li $v0, 2
	syscall
	j main_loop

fin:

	li $v0, 10 			# codigo de llamada para terminar
	syscall 			# llamada al sistema	


.data # declaracion del segmento de datos
	catetoRead0:.asciiz "\n Ingrese el primer cateto: "
	catetoRead1:.asciiz "\n Ingrese el segundo cateto: "
	hipotenusaPrint: .asciiz "\n La hipotenusa correspondiente es: "
	infinityPrint: .asciiz  "\n Para realizar ese calculo requiere mas precision "
	out1: .asciiz "\n Para catetos iguales a 3 y 4, la hipotenusa es "
	out2: .asciiz "\n Para catetos iguales a 30,5 y 42,7, la hipotenusa es "
	n1: .float 3,0
	n2: .float 4,0
	n3: .float 30,5
	n4: .float 42,7