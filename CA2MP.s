.text 
main: # label for main
	lw $t0, PAL
	la $t1, DIRACU
	li $t3, 0 #i
	li $t7, 0 #carry

	li $v0, 4
	la $a0, dirac # load adress
	syscall
printLoopDirAcu:
	sltu $t4, $t3, $t0
	beq $t4, $zero, loop0

	sll $t4, $t3, 2	 
	add $t5, $t4, $t1 # $t5=(DIRACU[i])

	lw $t6,0($t5) # $t6=DIRACU[i]

	li $v0, 1
	move $a0, $t6
	syscall

	li $v0, 4
	la $a0, comma
	syscall

	addi $t3, $t3, 1
	j printLoopDirAcu


loop0:
	li $t3, 0
	li $t7, 1

loop:
	sltu $t4, $t3, $t0
	beq $t4, $zero, endSum

	sll $t4, $t3, 2	 
	addu $t5, $t4, $t1 # $t5=(DIRACU[i])

	lw $t6,0($t5) # $t6=DIRACU[i]

	nor $t4, $t6, $zero #~DIRACU[i]
	addu $t8, $t4, $t7 #$t8=~DIRACU[i] + carry 
	#nor $t6, $t4, $zero #Checking Overflow
	sltu $t6, $t6, $t7
	bne $t6,$zero,OverflowC 
	
	li $t7, 0	
	j store

OverflowC:
	addi $t7, $0, 1 

store:
	sw $t8, 0($t5)
	addi $t3, $t3, 1
	j loop


endSum:
	li $t3, 0

	li $v0, 4
	la $a0, dirac # load adress
	syscall
printLoopDirAcu2:


	sltu $t4, $t3, $t0
	beq $t4, $zero, endProg

	sll $t4, $t3, 2	 
	add $t5, $t4, $t1 # $t5=(DIRACU[i])

	lw $t6,0($t5) # $t6=DIRACU[i]

	li $v0, 1
	move $a0, $t6
	syscall

	li $v0, 4
	la $a0, comma
	syscall

	addi $t3, $t3, 1
	j printLoopDirAcu2

endProg:
	li $v0, 10
	syscall


.ktext 0x80000180
	mfc0 $k0, $14
	addi $k0, $k0,4
	jr $k0


.data # declare data segment
	PAL:.word 4 # initialize x to 10
	DIRACU:.word 0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF 
	comma: .asciiz ","
	dirac: .asciiz "\nDIRACU: "
