.text # declare text segment

raiz:#procedimiento q recibe un x positivo, punto flotante en $f4
	 #y devuelve la raiz en $f2, calculada con el metodo newtoniano
	addi    $t2,  $zero, 0x2 #cargado del 2 usado
	mtc1    $t2,  $f10       #en el algoritmo
	cvt.s.w $f10, $f10
	li    $t0, 0x0			 #inicio de contador
	li    $t1, 0x14          # maxima cantidad de iteraciones
	div.s $f2, $f4, $f10     #semilla inicial = N/2

###########
loop_raiz:
	div.s $f9, $f4, $f2
	add.s $f2, $f2, $f9
	div.s $f2, $f2, $f10
	addi  $t0, $t0, 0x1
	beq   $t0, $t1, fin_raiz
	j     loop_raiz   
###########
fin_raiz: jr $ra 


hipotenusa:#recibe en $f4, y $f5 los catetos y devuelve en $v0 la hipotenusa
	mul.s $f4,$f4,$f4
	mul.s $f5,$f5,$f5
	add.s $f4,$f4,$f5
	addi  $sp,$sp, -4
	sw    $ra, 0($sp)
	jal   raiz   #raiz deja la hip en $f2
	mfc1  $v0,$f2
	lw    $ra,0($sp)
	addi  $sp,$sp, 4
	jr    $ra

main: # label for main

	# Impresion del mensaje de solicitud de entrada
	li $v0, 4
	la $a0, catetoRead0 # carga direccion del string
	syscall

	# Lectura del cateto 0 (flotante) 
	li $v0, 6
	syscall
	mov.s $f4,$f0

	# Impresion del mensaje de solicitud de entrada
	li $v0, 4
	la $a0, catetoRead1 # carga direccion del string
	syscall

	# Lectura del cateto 1 (flotante) 
	li $v0, 6
	syscall
	mov.s $f5,$f0

	jal hipotenusa
	mtc1 $v0,$f12

	# Impresion de la hipotenusa (flotante)
	# Impresion del mensaje de salida
	li $v0, 4
	la $a0, hipotenusaPrint # carga direccion del string
	syscall
	#######
	li $v0, 2
	syscall

	li   $t0, 0x7E800000
	mtc1 $t0, $f12
	li   $t0, 0x44800000
	mtc1 $t0, $f13
	mul.s $f12,$f12,$f13
	#add.s $f12,$f12,$f10

	li $v0, 2
	syscall


fin:

	li $v0, 10 			# codigo de llamada para terminar
	syscall 			# llamada al sistema	


.data # declaracion del segmento de datos
	catetoRead0:.asciiz "\n Ingrese el primer cateto: "
	catetoRead1:.asciiz "\n Ingrese el segundo cateto: "
	hipotenusaPrint: .asciiz "\n La hipotenusa correspondiente es: "
