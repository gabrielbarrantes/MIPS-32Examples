#######################################################
# Gabriel Barrantes Rodas B30904 grupo 1              #
# Tarea8: Se implemento un algoritmo que calcula las  #
# caracteristicas de un triangulo dados sus lados,    #
# mediante un procedimiento algebraico se obtuvieron  #
# formulas para cada dato solicitado, como sumas,     #
# multiplicaciones, divisiones y raiz de las entradas #
# con lo que solo se implemento la formula en punto   #
# flotante y se uso el procedimiento raiz implementado#
# en alguna tarea anterior.                           #
#######################################################
.text # declare text segment

######################################
##                                  ##
##               Raiz               ##
##                                  ##
######################################
raiz:#procedimiento q recibe un x no negativo, punto flotante en $f4
	 #y devuelve la raiz en $f2, calculada con el metodo newtoniano
	mtc1    $zero,$f5
	cvt.s.w $f5,  $f5
	c.eq.s  $f5,  $f4
	bc1f    entradaPositiva_raiz
	add.s   $f2, $f5,$f5
	j       fin_raiz
###########
entradaPositiva_raiz:
	addi    $t2,  $zero, 0x2 #cargado del 2 usado
	mtc1    $t2,  $f10       #en el algoritmo
	cvt.s.w $f10, $f10       #y convertido en flotante
	li    $t0, 0x0			 #inicio de contador
	li    $t1, 0x14          # maxima cantidad de iteraciones
	div.s $f2, $f4, $f10     #semilla inicial = N/2
###########
loop_raiz:
	div.s $f9, $f4, $f2
	add.s $f2, $f2, $f9
	div.s $f2, $f2, $f10
	addi  $t0, $t0, 0x1
	beq   $t0, $t1, fin_raiz
	j     loop_raiz   
###########
fin_raiz: jr $ra

#############################################
## Calculadora de Alturas                  ##
## entrada en a0, a1, a2 en punto flotante ##
##             a,  b,  c                   ##
##             los lados de un triangulo   ##
## retorna en v0 la altura sobre a         ##
#############################################
altura:
	addi    $t0,  $zero, 4 #cargado del 4 a usar
	mtc1    $t0,  $f12
	cvt.s.w $f12, $f12
#############################
	mtc1  $a0,  $f8
	mtc1  $a1,  $f9
	mtc1  $a2,  $f10
	mul.s $f8,  $f8,  $f8 #a^2
	mul.s $f9,  $f9,  $f9 #b^2
	mul.s $f10, $f10, $f10#c^2
	add.s $f11, $f8,  $f9
	sub.s $f11, $f11, $f10#resto c^2
	mul.s $f11, $f11, $f11
	mul.s $f8,  $f8,  $f12#4a^2 en f8
	div.s $f11, $f11, $f8
	sub.s $f4,  $f9,  $f11
	###################
	addi  $sp, $sp, -4  #llamado a raiz
	sw    $ra, 0($sp)
	jal   raiz
	lw    $ra, 0($sp)
	addi  $sp, $sp, 4
	###################
	mfc1  $v0,  $f2
	jr    $ra

###############################################
##    Calculadora de Area                    ##
##    entrada en a0,   a1 en punto flotante  ##
##               base, altura de un triangulo##               ##
##    retorna en v0 el area                  ##
###############################################
area:
	addi    $t0,  $zero, 2 #cargado del 2 a usar
	mtc1    $t0,  $f12
	cvt.s.w $f12, $f12
#############################
	mtc1    $a0, $f8
	mtc1    $a1, $f9
	mul.s   $f2, $f8, $f9  #multiplica base por altura
	div.s   $f2, $f2, $f12 #divide entre 2
	mfc1    $v0, $f2
	jr      $ra
#######################################

##########################################
##                                      ##
## Triangulo: lee tres lados y despliega##
## la informacion                       ##
##                                      ##
##########################################
triangulo:
    addi $sp, $sp, -12  #usaremos s0,s1,s2 
	sw   $s0, 0($sp)    #los apilamos
	sw   $s1, 4($sp)
	sw   $s2, 8($sp)
#######################
triangulo_ladoRead0:    #leemos el primer lado 
	addi $v0, $zero, 4  #y validamos q sea positivo
	la   $a0, ladoRead0 # carga direccion del string
	syscall

	li $v0, 6
	syscall
	mfc1 $s0, $f0

	mtc1    $zero,$f5   #comparamos para ver si es positivo
	cvt.s.w $f5,  $f5   
	c.lt.s  $f5,  $f0
	bc1t    triangulo_ladoRead1
	addi    $v0, $zero, 4
	la      $a0, ladoRead0_error # carga direccion del string
	syscall
	j       triangulo_ladoRead0
######################
######################
triangulo_ladoRead1:    #leemos el segundo lado
	addi $v0, $zero, 4
	la   $a0, ladoRead1 # carga direccion del string
	syscall

	li $v0, 6
	syscall
	mfc1 $s1, $f0

    mtc1    $zero,$f5
	cvt.s.w $f5,  $f5
	c.lt.s  $f5,  $f0
	bc1t    triangulo_ladoRead2
	addi    $v0, $zero, 4
	la      $a0, ladoRead0_error # carga direccion del string
	syscall
	j       triangulo_ladoRead1
######################
triangulo_ladoRead2:    #leemos el tercer lado
	addi $v0, $zero, 4
	la   $a0, ladoRead2 # carga direccion del string
	syscall

	li $v0, 6
	syscall
	mfc1 $s2, $f0

	mtc1    $zero,$f5
	cvt.s.w $f5,  $f5
	c.lt.s  $f5,  $f0
	bc1t    verificar_triangular
	addi    $v0, $zero, 4
	la      $a0, ladoRead0_error # carga direccion del string
	syscall
	j       triangulo_ladoRead2
#############################
#############################
verificar_triangular:  #verificamos q se cumplan 
	mtc1   $s0, $f8    #las tres desigualdades triangulares
	mtc1   $s1, $f9
	mtc1   $s2, $f10

	add.s  $f11, $f9, $f10
	c.lt.s $f8, $f11
	bc1t   paso1
	addi   $v0, $zero, 4
	la     $a0, noTriangular # carga direccion del string
	syscall
	j      triangulo_ladoRead0
paso1:
	add.s  $f11, $f8, $f10
	c.lt.s $f9, $f11
	bc1t   paso2
	addi   $v0, $zero, 4
	la     $a0, noTriangular # carga direccion del string
	syscall
	j      triangulo_ladoRead0
paso2:
	add.s  $f11, $f9, $f8
	c.lt.s $f10, $f11
	bc1t   paso3
	addi   $v0, $zero, 4
	la     $a0, noTriangular # carga direccion del string
	syscall
	j      triangulo_ladoRead0
paso3:
#############################
## perimetro
	addi  $v0, $zero, 4     #sumamos los lados e imprimimos
	la    $a0, perimetroOut # carga direccion del string
	syscall
	mtc1  $s0,  $f8
	mtc1  $s1,  $f9
	mtc1  $s2,  $f10
	add.s $f12, $f8, $f9
	add.s $f12, $f12, $f10
	addi  $v0, ,$zero, 2
	syscall
###Alturas            #calculamos las 3 alturas con el procedimiento respectivo
	addi $v0, $zero, 4
	la   $a0, altura0 # carga direccion del string
	syscall
	move $a0, $s0
	move $a1, $s1
	move $a2, $s2
	addi $sp, $sp, -4
	sw   $ra, 0($sp)
	jal  altura
	lw   $ra, 0($sp)
	addi $sp, $sp, 4
	mtc1 $v0, $f12
	addi $v0, $zero, 2
	syscall
#
	addi $v0, $zero, 4
	la   $a0, altura1 # carga direccion del string
	syscall
	move $a0, $s1
	move $a1, $s0
	move $a2, $s2
	addi $sp, $sp, -4
	sw   $ra, 0($sp)
	jal  altura
	lw   $ra, 0($sp)
	addi $sp, $sp, 4
	mtc1 $v0, $f12
	addi $v0, $zero, 2
	syscall
#
	addi $v0, $zero, 4
	la   $a0, altura2 # carga direccion del string
	syscall
	move $a0, $s2
	move $a1, $s1
	move $a2, $s0
	addi $sp, $sp, -4
	sw   $ra, 0($sp)
	jal  altura
	lw   $ra, 0($sp)
	addi $sp, $sp, 4
	mtc1 $v0, $f12
	move $a0, $v0
	addi $v0, $zero, 2
	syscall
#area, Calculamos el area con el procedimiento respectivo
	move $a1, $s2  
	addi $sp, $sp, -4
	sw   $ra, 0($sp)
	jal  area
	lw   $ra, 0($sp)
	addi $sp, $sp, 4
	mtc1 $v0, $f12
	addi $v0, $zero, 4
	la   $a0, areaOut # carga direccion del string
	syscall
	addi $v0, $zero, 2
	syscall
#clasificacion/escaleno/isoceles/equilatero
################################
	addi   $v0, $zero, 4
	la     $a0, tipoTriangulo  # carga direccion del string
	syscall

	mtc1   $s0, $f8
	mtc1   $s1, $f9
	mtc1   $s2, $f10

	c.eq.s $f8, $f9
	bc1f   noEquilatero
	c.eq.s $f9, $f10
	bc1f   noEquilatero
	addi   $v0, $zero, 4
	la     $a0, equilatero  # carga direccion del string
	syscall
	j      triangulo_salir
noEquilatero:
	c.eq.s $f8, $f10
	bc1t   siIsoceles
	c.eq.s $f8, $f9
	bc1t   siIsoceles
	c.eq.s $f9, $f10
	bc1t   siIsoceles
	j      noIsoceles
siIsoceles:
	addi   $v0, $zero, 4
	la     $a0, isoceles  # carga direccion del string
	syscall
	j      triangulo_salir
noIsoceles:
	addi   $v0, $zero, 4
	la     $a0, escaleno  # carga direccion del string
	syscall

################################
triangulo_salir:
	lw   $s0, 0($sp)  #desapilamos los $s
	lw   $s1, 4($sp)
	lw   $s2, 8($sp)
	addi $sp, $sp, 12
	jr $ra

#########################################
#                                       #
#                  MAIN                 #
#                                       #
#########################################
main: # label for main

	jal triangulo #llamada al programa triangulo

fin:

	li $v0, 10 	# codigo de llamada para terminar
	syscall 	# llamada al sistema	


.data # declaracion del segmento de datos
	ladoRead0: .asciiz "\n Ingrese el primer lado (a): "
	ladoRead1: .asciiz "\n Ingrese el segundo lado (b): "
	ladoRead2: .asciiz "\n Ingrese el tercer lado (c): "
	ladoRead0_error: .asciiz "\n Lado ingresado negativo, intente de nuevo \n"
	noTriangular: .asciiz "\n Los lados no pueden formar un triangulo, intente de nuevo \n"
	perimetroOut:.asciiz "\n El perimetro es: "
	altura0: .asciiz "\n La altura sobre a es: "
	altura1: .asciiz "\n La altura sobre b es: "
	altura2: .asciiz "\n La altura sobre c es: "
	areaOut: .asciiz "\n El area es: "
	tipoTriangulo: .asciiz "\n El tipo de triangulo es: "
	equilatero: .asciiz "equilatero"
	isoceles:   .asciiz "isoceles"
	escaleno:   .asciiz "escaleno"
