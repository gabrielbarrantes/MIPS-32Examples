#######################################################
# Gabriel Barrantes Rodas B30904 grupo 1              #
# Tarea1: impresion sucesion de fibonachi, se lee un  #
# entero, se almacena en t3 y se utiliza como contador#
# luego en t0 y t1 se almacenan los ultimos dos       #
# terminos calculados q se utilizan para el calculo   #
# del siguiente termino que se imprime y sustituye    # 
# al ultimo termino en t1, mientras que el            # 
# termino en t0 es sustituido por el termino en t1    #
#######################################################
#ddd
.text # declare text segment

main: # label for main

	# Impresion del mensaje de solicitud de entrada
	li $v0, 4
	la $a0, nread # carga direccion del string
	syscall

	# Lectura del n 
	li $v0, 5
	syscall

	move $t3,$v0 #almacenaje de n en t3 para su uso
                 #como contador
	li   $t0,0x1 #inicializamos los terminos iniciales 
	li   $t1,0x1 #de la sucesion 

	li   $v0, 1  #imprimimos el primer termino
	add  $a0, $t0, $zero
	syscall

loop:
	addi $t3,$t3,-1   #decremento del contador
	beq  $t3,$zero,fin#si llega a cero, ya terminamos

	la   $a0,coma     #impresion de la coma
	li   $v0,4
	syscall

	add  $t2,$t0,$t1  #calculo el siguiente fibonachi
	move $t0,$t1      #y almacenaje en t0 y t1
	move $t1,$t2

	# Impresion del entero siguiente en la sucesion
	li   $v0, 1
	add  $a0, $t0, $zero
	syscall

	j    loop

fin:

	li $v0, 10 			# codigo de llamada para terminar
	syscall 			# llamada al sistema	


.data # declaracion del segmento de datos
	nread: .asciiz "\n Ingrese un natural 0<n<46: "
	coma:  .asciiz ", "
