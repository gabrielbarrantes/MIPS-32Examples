#floatExample.s

.data
	myFloat: .float 1500.1500 #Variable flotante
	myInt: 	.word 15	#Variable Entera

	d1: .asciiz "\nDigite un número (N1):"
	m0: .asciiz "\n Número ingresado (N1): "
	m1: .asciiz "\n Leer una variable .float en registro FP: "
	m2: .asciiz "\n Copiar un entero a un registro FP: "
	m3: .asciiz "\n Convertir un valor entero en un registro FP en un valor en FP (N2): "
	m4: .asciiz "\n división de N1/N2: "
	m5: .asciiz "\n división de N1/0: "
	m6: .asciiz "\n división de 0/0: "
	m7: .asciiz "\n Resultado N1/N2 en un registro entero sin Convertir el resultado antes a entero: "
	m8: .asciiz "\n Resultado N1/0  en un registro entero sin Convertir el resultado antes a entero: "


.text
main:
	li $v0, 4
	la $a0, d1
	syscall

	li $v0, 6 #Lee un float que queda en $f0
	syscall	


	lwc1 $f1, myFloat #Leer una variable flotante 
	
	lw $t0, myInt #Leer una variable entera

	mtc1 $t0, $f6 #Mover un registro entero a uno flotante
	mtc1 $t0, $f2 #Mover un registro entero a uno flotante
	cvt.s.w $f2, $f2 #Convierte un valor entero en un registro punto flotante en un valor en punto flotante

	mtc1 $0, $f7 #También se puede con el registro zero
	cvt.s.w $f7, $f7

	div.s $f3, $f0, $f2 #división de flotantes

	div.s $f4, $f1, $f7 #división entre cero

	div.s $f5, $f7, $f7 #división cero entre cero

	mfc1 $t1, $f4 #Mover el contenido de un registro FP a uno entero
	mfc1 $t2, $f5

print:
	li $v0, 4
	la $a0, m0
	syscall

	li $v0, 2
	mov.s $f12, $f0
	syscall	

	li $v0, 4
	la $a0, m1
	syscall

	li $v0, 2
	mov.s $f12, $f1
	syscall	


	li $v0, 4
	la $a0, m2
	syscall

	li $v0, 2
	mov.s $f12, $f6
	syscall


	li $v0, 4
	la $a0, m3
	syscall

	li $v0, 2
	mov.s $f12, $f2
	syscall


	li $v0, 4
	la $a0, m4
	syscall

	li $v0, 2
	mov.s $f12, $f3
	syscall


	li $v0, 4
	la $a0, m5
	syscall

	li $v0, 2
	mov.s $f12, $f4
	syscall


	li $v0, 4
	la $a0, m6
	syscall

	li $v0, 2
	mov.s $f12, $f5
	syscall


	li $v0, 4
	la $a0, m7
	syscall

	li $v0, 1
	move $a0, $t1
	syscall


	li $v0, 4
	la $a0, m8
	syscall

	li $v0, 1
	move $a0, $t2
	syscall

	li $v0, 10 #STOP
	syscall
