.text 
main: # label for main
	lw $t0, PAL
	la $t1, DIRACU
	la $t2, DIROPE
	li $t3, 0 #i
	li $t7, 0 #carry

	li $v0, 4
	la $a0, dirac # load adress
	syscall
printLoopDirAcu:
	sltu $t4, $t3, $t0
	beq $t4, $zero, printLoopDirOpe0

	sll $t4, $t3, 2	 
	add $t5, $t4, $t1 # $t5=(DIRACU[i])
	add $t4, $t4, $t2 # $t4=(DIROPE[i])

	lw $t4,0($t4) # $t4=DIROPE[i]
	lw $t6,0($t5) # $t6=DIRACU[i]

	li $v0, 1
	move $a0, $t6
	syscall

	li $v0, 4
	la $a0, comma
	syscall

	addi $t3, $t3, 1
	j printLoopDirAcu

printLoopDirOpe0:
	li $t3, 0
	li $v0, 4
	la $a0, dirop # load adress
	syscall	
printLoopDirOpe:
	sltu $t4, $t3, $t0
	beq $t4, $zero, loop0

	sll $t4, $t3, 2	 
	add $t5, $t4, $t1 # $t5=(DIRACU[i])
	add $t4, $t4, $t2 # $t4=(DIROPE[i])

	lw $t4,0($t4) # $t4=DIROPE[i]
	lw $t6,0($t5) # $t6=DIRACU[i]

	li $v0, 1
	move $a0, $t4
	syscall

	li $v0, 4
	la $a0, comma
	syscall

	addi $t3, $t3, 1
	j printLoopDirOpe


loop0:
	li $t3, 0

loop:
	sltu $t4, $t3, $t0
	beq $t4, $zero, endSum

	sll $t4, $t3, 2	 
	add $t5, $t4, $t1 # $t5=(DIRACU[i])
	add $t4, $t4, $t2 # $t4=(DIROPE[i])

	lw $t4,0($t4) # $t4=DIROPE[i]
	lw $t6,0($t5) # $t6=DIRACU[i]

	addu $t8, $t4, $t7 #$t8=DIROPE[i] + carry 
	nor $t4, $t4, $zero #Checking Overflow
	sltu $t4, $t4, $t7
	bne $t4,$zero,OverflowC 
	
	li $t7, 0	
	j next

OverflowC:
	addi $t7, $0, 1 

next:
	addu $t4, $t8, $t6 # $t4 = DIROPE[i] + carry + DIRACU[i] 
	nor $t8, $t8, $zero #Checking carry
	sltu $t8, $t8, $t6
	bne $t8,$zero,OverflowS 

	j store

OverflowS:
	addi $t7, $t7, 1 

store:
	sw $t4, 0($t5)
	addi $t3, $t3, 1
	j loop


endSum:
	li $t3, 0

	li $v0, 4
	la $a0, dirac # load adress
	syscall
printLoopDirAcu2:


	sltu $t4, $t3, $t0
	beq $t4, $zero, endProg

	sll $t4, $t3, 2	 
	add $t5, $t4, $t1 # $t5=(DIRACU[i])
	add $t4, $t4, $t2 # $t4=(DIROPE[i])

	lw $t4,0($t4) # $t4=DIROPE[i]
	lw $t6,0($t5) # $t6=DIRACU[i]

	li $v0, 1
	move $a0, $t6
	syscall

	li $v0, 4
	la $a0, comma
	syscall

	addi $t3, $t3, 1
	j printLoopDirAcu2

endProg:
	li $v0, 10
	syscall


.ktext 0x80000180
	mfc0 $k0, $14
	addi $k0, $k0,4
	jr $k0


.data # declare data segment
	PAL:.word 4 # initialize x to 10
	DIRACU:.word 0x00000001,0xF0000000,0xF0000000,0x40000000 
	DIROPE:.word 0x00000001,0x80000000,0x80000000,0x40000000
	comma: .asciiz ","
	dirac: .asciiz "\nDIRACU: "
	dirop: .asciiz "\nDIROPE: "