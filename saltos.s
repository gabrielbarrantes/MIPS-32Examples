
.text # declare text segment
main: # label for main

    li $t0, 9

    li $t1, 7

    blt $t1,$t0, salto

    # Print message for int read
    li $v0, 4
    la $a0, yread # load adress
    syscall

salto:
    # Print message for int read
    li $v0, 4
    la $a0, xread # load adress
    syscall

    li $v0, 10         # call code for terminate
    syscall            # system call   


.data # declare data segment
    myString: .asciiz "\nThe cocient of x/y is: "
    myString2: .asciiz "\nThe reminder of x/y is: "
    xread: .asciiz    "\n Enter x value: "
    yread: .asciiz    "\n Enter y value: "