########################################################
# Gabriel Barrantes Rodas B30904 grupo 1               #
# Tarea 6,7: Se implementa la calculadora, procesando  #
# un string de entrada, primero con un procedimiento q #
# determina su tamano y luego en un "case" se determina#
# si es una operacion valida o un numero, si es un     #
# numero se llama a un procedimiento que convierte     #
# el string en numero y se apila. Se uso como pila de  #
# la calculadora la pila global.                       #
########################################################
.text # declare text segment

######################################
##                                  ##
##           GetNumber              ##
##                                  ##
######################################
getNumber:#recibe en $a0 la string y en $a1 la longitud
		  #el numero puede ser negativo, en ese caso $a1
		  #tambien suma una unidad por el -
		  #devuelve en $v0 el entero, si el string es invalido 
		  #en $v1 se devuelve un 1 (true)
	addi $v1, $zero, 0x0  
	addi $t7, $zero, 0x0
	lb   $t1, 0($a0)
	addi $t5, $zero, 0x2D
	bne  $t1, $t5, getNumber_positivo
	addi $t7, $zero, 0x1
	addi $a0, $a0,  1
	addi $a1, $a1, -1
getNumber_positivo:
	addi $v0, $zero, 0x0 #seteado de v0 a cero
	addi $t2, $zero, 0xA #cargado del 10 multiplicador
##########################
getNumber_loop1:
	lb   $t1, 0($a0)
	addi $t1, $t1, -48
#########################
	bgez $t1, getNumber_esCasiValido
	addi $v1, $zero, 0x1  #codigo de error
	j    getNumber_fin_1
getNumber_esCasiValido:
	sub  $t5, $t2, $t1
	bgtz $t5, getNumber_esValido
	addi $v1, $zero, 0x1  #codigo de error
	j    getNumber_fin_1
#########################
getNumber_esValido:	
	mult $v0, $t2
	mflo $v0
	addu $v0, $v0, $t1
	addi $a0, $a0,  1
	addi $a1, $a1, -1
	beq  $a1, $zero, getNumber_fin_0
	j    getNumber_loop1
##########################
getNumber_fin_0:
	beq  $t7, $zero, getNumber_fin_1 
	sub  $v0, $zero, $v0
getNumber_fin_1:jr  $ra
######################################
##                                  ##
##               Raiz               ##
##                                  ##
######################################
raiz:#procedimiento q recibe un x no negativo, punto flotante en $f4
	 #y devuelve la raiz en $f2, calculada con el metodo newtoniano
	mtc1    $zero,$f5
	cvt.s.w $f5,  $f5
	c.eq.s  $f5,  $f4
	bc1f    entradaPositiva_raiz
	add.s   $f2, $f5,$f5
	j       fin_raiz
###########
entradaPositiva_raiz:
	addi    $t2,  $zero, 0x2 #cargado del 2 usado
	mtc1    $t2,  $f10       #en el algoritmo
	cvt.s.w $f10, $f10       #y convertido en flotante
	li    $t0, 0x0			 #inicio de contador
	li    $t1, 0x14          # maxima cantidad de iteraciones
	div.s $f2, $f4, $f10     #semilla inicial = N/2
###########
loop_raiz:
	div.s $f9, $f4, $f2
	add.s $f2, $f2, $f9
	div.s $f2, $f2, $f10
	addi  $t0, $t0, 0x1
	beq   $t0, $t1, fin_raiz
	j     loop_raiz   
###########
fin_raiz: jr $ra 
######################################
##                                  ##
##           GetNUMCHAR             ##
##                                  ##
######################################
getNUMCHAR:#recibe en $a0 la direccion del string y 
		   #devuelve en $v0 el numero de caracteres
	addi $v0, $zero, 0x0 #inicio del contador
getNUMCHAR_loop1:
	lw   $t1, 0($a0)
##########################
getNUMCHAR_loop2:
	addi $t2, $zero, 0xFF
	addi $t3, $zero, 0x0A #caracter de final de linea
	and  $t4, $t2, $t1    #vemos el primer byte
	beq  $t4, $t3,   getNUMCHAR_fin
	beq  $t4, $zero, getNUMCHAR_fin
	addi $v0, $v0, 0x1    #se suma 1 al contador
	srl  $t1, $t1, 0x8    #se rota un byte
	bne  $t1, $zero, getNUMCHAR_loop2
##########################
	addi $a0, $a0, 0x4    #se pasa a la siguiente palabra
	j getNUMCHAR_loop1
getNUMCHAR_fin: jr $ra
######################################
##                                  ##
##           Calculadora            ##
##                                  ##
######################################
Calculadora:
###################################
	addi $v0, $zero, 4         #Imprimir bienvenida  
	la   $a0, mensajeBienvenida#e instucciones
	syscall
###################################
	addi    $sp, $sp, -4 #apilo un cero
	move    $s3, $sp     #como operando inicial
	mtc1    $zero, $f4
	cvt.s.w $f2, $f4
	mfc1    $t0, $f2
	sw      $t0, 0($sp)
##################################
loop_Calculadora:
	li $v0, 8      #lectura de entrada en buffer
	la $a0, buffer 
	li $a1, 16
	syscall
#############################
	la   $a0, buffer   #llamado a getNUMCHAR para
	lb   $s0, 0($a0)   #saber el largo
	addi $sp, $sp, -4
	sw   $ra, 0($sp)
	jal  getNUMCHAR
	lw   $ra, 0($sp)
	addi $sp, $sp, 4
#############################
	addi $t0, $zero, 0x1   #si la longitud no es 1 
	bne  $v0, $t0, esNumero#es un posible numero
#############################
	addi $t0, $s0, -48     #vemos si es un digito
	bgez $t0, esCasiValido #si no, es una posible
	j    esCaracter        #operacion
esCasiValido:
	addi $t1, $zero, 0xA
	sub  $t0, $t1, $t0
	bgtz $t0, esNumero
##############################
esCaracter:
    addi $t0, $zero, 32 #comparamos para ver si es espacio
    bne  $s0, $t0, noEsEspacio
    lw   $t0, 0($sp)    #si es espacio, imprimimos el tope 
    mtc1 $t0, $f12
    addi $v0, $zero, 2  #de la pila
    syscall
    la   $a0, nuevaLinea
    addi $v0, $zero, 4   
    syscall
    j    loop_Calculadora
noEsEspacio:
####################################
	addi  $t0, $zero, 43 ##comparamos para ver si es suma
	bne   $s0, $t0, noSuma
#
	sub   $t0, $sp, $s3
	bgez  $t0, noHayOperandos
#
	lw    $s1, 0($sp)
	lw    $s2, 4($sp)
	mtc1  $s1, $f4
	mtc1  $s2, $f5
	add.s $f2, $f4, $f5
	mfc1  $v0, $f2
	sw    $v0, 4($sp)
	addi  $sp, $sp, 4
	j    loop_Calculadora
noSuma:
	addi  $t1, $zero, 45 #comparamos para ver si es resta
	bne   $s0, $t1, noResta
	sub   $t0, $sp, $s3
	bgez  $t0, noHayOperandos
	lw    $s1, 0($sp)
	lw    $s2, 4($sp)
	mtc1  $s1, $f4
	mtc1  $s2, $f5
	sub.s $f2, $f4, $f5
	mfc1  $v0, $f2
	sw    $v0, 4($sp)
	addi  $sp, $sp, 4
	j    loop_Calculadora
noResta:
	addi  $t2, $zero, 42 ##comparamos para ver si es multiplicacion
	bne   $s0, $t2, noMultiplica
	sub   $t0, $sp, $s3
	bgez  $t0, noHayOperandos
	lw    $s1, 0($sp)
	lw    $s2, 4($sp)
	mtc1  $s1, $f4
	mtc1  $s2, $f5
	mul.s $f2, $f4, $f5
	mfc1  $v0, $f2
	sw    $v0, 4($sp)
	addi  $sp, $sp, 4
	j    loop_Calculadora	
noMultiplica:
	addi  $t3, $zero, 47 #dividir
	bne   $s0, $t3, noDivide
	sub   $t0, $sp, $s3
	bgez  $t0, noHayOperandos
	lw    $s1, 0($sp)
	lw    $s2, 4($sp)
	mtc1  $s1, $f4
	mtc1  $s2, $f5
	div.s $f2, $f4, $f5
	mfc1  $v0, $f2
	sw    $v0, 4($sp)
	addi  $sp, $sp, 4
	j    loop_Calculadora
noDivide:
	addi  $t6, $zero, 80  #cuadrado
	addi  $t7, $zero, 112 #cuadrado
	beq   $s0, $t6, Cuadrado
	beq   $s0, $t7, Cuadrado
	j     noCuadrado
Cuadrado:
	lw    $s1, 0($sp)
	mtc1  $s1, $f4
	mul.s $f2, $f4, $f4
	mfc1  $v0, $f2
	sw    $v0, 0($sp)
	j    loop_Calculadora
noCuadrado:
	addi  $t8, $zero, 83 #raiz
	addi  $t9, $zero, 115#raiz
	beq   $s0, $t8, Raiz
	beq   $s0, $t9, Raiz
	j     noRaiz
Raiz:

	lw    $s1, 0($sp)
	mtc1  $s1, $f4    #poner en f4 el operando
####
	mtc1    $zero,$f5 #verificamos q la entrada 
	cvt.s.w $f5, $f5  #sea positiva
	c.le.s  $f5, $f4
	bc1t  entrada_raiz_valida#si no imprimimos error
	addi $v0, $zero, 4
	la   $a0, raiz_negativa
	syscall
	j    loop_Calculadora
####
entrada_raiz_valida:
	addi  $sp, $sp, -4
	sw    $ra, 0($sp)
	jal   raiz
	lw    $ra, 0($sp)
	addi  $sp, $sp, 4
	mfc1  $v0, $f2
	sw    $v0, 0($sp)
	j    loop_Calculadora
noRaiz:
	addi  $t4, $zero, 67 #clear
	addi  $t5, $zero, 99 #clear
	beq   $s0, $t4, clear
	beq   $s0, $t5, clear
	j     noClear
############################
clear:
    move  $sp, $s3
    sw    $zero, 0($sp)
    j     loop_Calculadora
############################
noClear:
	addi  $t4, $zero, 69 #clear
	addi  $t5, $zero, 101 #clear
	beq   $s0, $t4, Calculadora_fin
	beq   $s0, $t5, Calculadora_fin
entrada_error:        #si no es alguna entrada valida 
	addi $v0, $zero, 4#se imprime error
	la   $a0, error
	syscall
	j    loop_Calculadora
noHayOperandos:
	addi $v0, $zero, 4#se imprime error
	la   $a0, noHayOperandos_String
	syscall
	j    loop_Calculadora
###############################
esNumero:

	addi $sp, $sp, -4
	sw   $ra, 0($sp)
	la   $a0, buffer
	jal  getNUMCHAR
	lw   $ra, 0($sp)
	addi $sp, $sp, 4
	move $a1, $v0
	la   $a0, buffer
	##
	addi $sp, $sp, -4
	sw   $ra, 0($sp)
	jal  getNumber
	lw   $ra, 0($sp)
	addi $sp, $sp, 4
	beq  $v1, $zero, valido
	addi $v0, $zero, 4 ##error
	la   $a0, error
	syscall
	j    loop_Calculadora

valido:
	mtc1    $v0, $f4
	cvt.s.w $f2, $f4 
	mfc1    $v0, $f2
	addi    $sp, $sp, -4
	sw      $v0, 0($sp)
    j       loop_Calculadora
    ##

Calculadora_fin: 
	move $sp, $s3
	addi $sp, $sp, 4
	jr $ra

#######################################
#                                     #
#                MAIN                 #
#                                     #
#######################################
main: # label for main


	jal Calculadora     #llamado a la calculadora


fin:

	li $v0, 10 			# codigo de llamada para terminar
	syscall 			# llamada al sistema	


.data # declaracion del segmento de datos
    buffer:.space  16
	raiz_negativa: .asciiz "\n Solicito la raiz de un negativo, un gatito murio \n"
	noHayOperandos_String: .asciiz "\nNo hay suficientes operandos apilados\n"
	mensajeBienvenida: .asciiz "\n Bienvenido a la calculadora polaca inversa MIPS \n
 Instrucciones para su uso: \n
 Ingrese un numero a la vez y presione enter, el numero sera apilado,
 inicialmente se apila un cero, si se hace un clear, se limpiara la 
 pila pero luego se apilara un cero.\n 
 Las operaciones binarias validas son: +, *, -, /\n
 Luego de ingresar una operacion, presione enter y esta se realizara inmediatamente\n
 C,c-->clear, P,p-->cuadrado, S,s-->raiz, E,e-->salir, space-->muestra ultimo resultado\n\n"
	error: .asciiz "\n Entrada invalida, un gatito murio \n"
	nuevaLinea: .asciiz "\n"