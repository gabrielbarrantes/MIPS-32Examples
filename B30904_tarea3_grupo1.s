#######################################################
# Gabriel Barrantes Rodas B30904 grupo 1              #
# Tarea3: Se implementa el algoritmo de la multiplica #
# cion usado el algoritmo del libro de texto          #
#######################################################
.text # declare text segment

##################################
#     Rutina multiplicadora      #
##################################
multiplicar: #recibe dos enteros con signo en $a0 y $a1
             #y devuelve su multiplicacion en $v0 y $v1
             #con $v0 la parte baja

    li   $t0, 0x80000000 #mascara para signo
    and  $t6, $a0, $t0   #signo de $a0
    and  $t7, $a1, $t0   #signo de $a1

    beq   $t6, $zero, noComplementar_a0 #si a0 es negativo 
    nor   $a0,$a0,$zero  #se niega y suma 1 
    addiu $a0,$a0, 0x1
noComplementar_a0:
    beq   $t7, $zero, noComplementar_a1 #si a1 es negativo
    nor   $a1,$a1,$zero  #se niega y suma 1 
    addiu $a1,$a1, 0x1
noComplementar_a1:
    xor  $t4, $t7,$t6    #signo del producto
########################################################
########################################################
##  Multiplicacion como si fueran positivos
    li   $v0, 0x0  #puesta a cero los registros de salida
    li   $v1, 0x0  #para comenzar el ciclo de sumas

    li   $t0, 0x0  #contador de iteraciones
    li   $t1, 0x20 #cantidad de iteraciones
    li   $t2, 0x1  #mascara para rotado a la derecha
                   #y control de sumas
loop_multiplicar:
    and  $t3,$a1,$t2 #t3 es cero si no hay que sumar
    beq  $t3,$zero,noSuma
    addu $v1,$v1,$a0 #se suma el multiplicando a la parte alta
noSuma:
    srl  $a1,$a1, 0x1#se rota el multiplicador
#   Rotado de la palabra concatenada v1v0  #
    and  $t3,$v1,$t2  #se rota v1v0, en t3 ponemos el
    sll  $t3,$t3, 0x1F#bit q pasa de v1 a v0 
    srl  $v0,$v0, 0x1 #y lo ponemos en la posicion 32
    srl  $v1,$v1, 0x1 #le sumamos t3 a v0 para incluir el bit
    addu $v0,$v0,$t3  #que viene de v1
########################
    addi $t0,$t0, 0x1 #aumentamos el contador
    beq  $t0,$t1,fin_multiplicar0 #si se hicieron 32 iteraciones salimos
    j    loop_multiplicar # si no regresamos al loop

fin_multiplicar0: #acondicionamiento de la salida con el signo
    beq  $t4,$zero,fin_multiplicar1#si el signo es cero, salimos
    nor  $v0,$v0,$zero # si no complementamos a 2
    nor  $v1,$v1,$zero 
    li   $t5, 0xFFFFFFFF
    bne  $v0,$t5, noCarry
    addu $v1,$v1, 0x1
noCarry:
    addu $v0,$v0, 0x1
fin_multiplicar1: jr $ra

################################
################################
#####                      #####
#####         MAIN         #####
#####                      #####
################################
################################
main:

    # Lectura del primer valor
    li $v0, 4
    la $a0, xread
    syscall
    # INT Read
    li $v0, 5
    syscall
    move $t0,$v0  #guardado en t0 del valor leido
    ##########################
    # Lectura del segundo valor
    li $v0, 4
    la $a0, yread # load adress
    syscall

    # INT Read
    li $v0, 5
    syscall

    move $t1,$v0  #guardado en t1 del valor leido
    ###########################
    move  $a0,$t0 #paso de los parametros para el
    move  $a1,$t1 #llamado a la multiplicacion

    jal   multiplicar #llamado a la rutina multiplicadora

    move  $t0,$v0 #mover los resultados a t0 y t1
    move  $t1,$v1

    # Impresion de los resultados
    #############################
    li $v0, 4
    la $a0, resultado # load adress
    syscall

    li $v0, 4
    la $a0, hi # load adress
    syscall

    # Print Hi
    li   $v0, 1
    move $a0, $t1
    syscall 

    # Print String
    li $v0, 4
    la $a0, lo 
    syscall

    # Print Lo
    li   $v0, 1
    move $a0, $t0
    syscall
    #############################
    #############################

    li $v0, 10      # call code for terminate
    syscall         # system call   


.data # declare data segment
    hi:       .asciiz "\n Hi "
    lo:       .asciiz "\n Lo "
    xread:    .asciiz "\n Ingrese el valor de x: "
    yread:    .asciiz "\n Ingrese el valor de y: "
    resultado:.asciiz "\n El producto de x con y es :\n "